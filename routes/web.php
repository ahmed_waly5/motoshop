<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/test_db',function(){
    if(DB::connection()->getDatabaseName())
    {
        echo "Yes! successfully connected to the DB: " . DB::connection()->getDatabaseName();
    }
});

Route::get('/', function () {
    return redirect('/home');
});


Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/motorcycle', 'MotorcycleController');
Route::get('/my-motorcycles', 'MotorcycleController@myMotorcycles');


@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
       <div class="container">
                  <div class="row">

           @if(count($records))
            @foreach($records as $record)

               <div class="col-md-4">
                   <div class="product-item">
                     <div class="pi-img-wrapper"  style="height: 350px;">
                       <img src="/images/{{$record->motorcyclePhotos[0]->photo_name}}" class="img-responsive" alt="{{$record->name}}" style="width: 400px">

                     </div>
                     <h3><a href="/motorcycle/{{$record->id}}">{{$record->name}}</a></h3>
                     <div class="pi-price">{{$record->price}} EGP</div>
                     <a class="btn add2cart">Added by {{$record->user->name}}</a>
                     <div class="sticker sticker-new"></div>
                   </div>
               </div>
            @endforeach

            @else
            <p>No Motorcycles are available for sale at the moment, Please visit us frquently</p>
          @endif
           </div>

       </div>
       {!! $records->render() !!}
    </div>
</div>
@endsection

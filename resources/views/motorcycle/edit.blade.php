@extends('...layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
       <div class="container">
          <div class="row">
            <div class="box-body">
              {{--{!!Form::open(['url'=>'/motorcycle','files' => true])!!}--}}
              {!!Form::model($record,['method' =>'PATCH', 'action'=>['MotorcycleController@update',$record->id],'files' => true])!!}
              {{--i created thse form particals to facilitate building forms as all of the applications i work on uses forms--}}

                @include('form_parcials.universal_text_field',['name'=>'name','label'=>'Name:'])
                @include('form_parcials.universal_textarea_field',['name'=>'description','label'=>'Description:'])
                @include('form_parcials.universal_integer_field',['name'=>'price','label'=>'Price:'])

                {!!Form::label('photos[]','Photos: ')!!}
                {!!Form::file('photos[]',array('multiple'=>true)) !!}
                @if ($errors->has('photos'))
                  <div class="error">
                    <i class="fa fa-times-circle"></i>
                    {{ $errors->first('photos') }}
                  </div>
                @endif

                @include('form_parcials.published_update' , [ 'value' => $record->published ])
                <div class="form-group">
                    {!!Form::label('sold','Mark as Sold: ')!!}
                    {!!Form::checkbox('sold',null) !!}
                </div>
                @include('form_parcials.submit_btn',['submit_btn_text'=>'Update'])
              {!!Form::close()!!}
            </div>

          </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

    <div class="container" style="max-width: 600px;">
      <h1>{{$record->name}}</h1>
      <hr/>
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->


        <!-- Wrapper for slides -->
        <div class="carousel-inner" style="height: 570px">
        @foreach($record->motorcyclePhotos as $index => $photo)
           <div class="item {{ $loop->first ? 'active' : '' }}">
           <img src="/images/{{$photo->photo_name}}"  style="width:100%;">
         </div>
        @endforeach

        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <div class="container">
              Description:
              <p>
              {!!$record->description!!}
              </p>
              Contact Details:

              <p>Phone: {{$record->user->phone}}</p>


               @if(count($record->user->motorcycles))
               Other Item from by {{$record->user->name}}:
                  @foreach($record->user->motorcycles as $index => $motorcycle)
                    <p><a href="/motorcycle/{{$motorcycle->id}}">{{$motorcycle->name}}</a></p>

                  @endforeach
                @endif
          </div>
    </div>


    </div>
</div>
@endsection
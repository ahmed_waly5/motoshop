<div class="form-group">
    {!!Form::label($name,$label)!!}
    {!!Form::text($name,null,['class'=> 'form-control','onkeypress'=>'return isNumberKey(event)']) !!}
</div>
@if ($errors->has($name))
    <div class="error">
        <i class="fa fa-times-circle"></i>
        {{ $errors->first($name) }}
    </div>
@endif


<script>

//to make that all the accepted characters are numbers only
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

## About This application:

This was a task completed by **[Ahmed Waly](https://www.linkedin.com/in/ahwaly/)**  as an evaluation task to join **[YallaEksab](https://yallaeksab.com//)**  team of development.
It's created using Laravel 5.6.


## Environment requirements to run this app.

- PHP 7.1.3
- MySQL 5.7
- Apache 2.4
- Composer
- Install and enable PHP extensions:
    - OpenSSL PHP Extension.
    - PDO PHP Extension.
    - Mbstring PHP Extension.
    - Tokenizer PHP Extension.
    - XML PHP Extension.
    - Ctype PHP Extension.
    - JSON PHP Extension.

## How to run this app.

- First clone this repo (https://bitbucket.org/ahmed_waly5/motoshop.git).
- Browse in the app root folder and using Composer run this command "composer install" and it will reads the composer.json file -which includes all the libraries and dependencies needed for the project-  from the current directory, processes it, and downloads and installs all the libraries and dependencies outlined in that file.
- Create database in MySQL with the name "rnd_motoshop_09" and import the attached database file named "rnd_motoshop_09.sql" which can be found in the app root folder.
- Update the .env file with your database credentials and host.
- Browse to the app root folder from the command line and run this command "php artisan serve", if all the instructions above are completed correctly you should see a message says "Laravel development server started: <http://127.0.0.1:8000>
" then the app would be accessible from any browser using the url "127.0.0.1:8000".
- From the homepage the user can browse existing items, register and add more items for sale of his own.

## Points to be done in the future.

- Change the description field to be a tiny text editor so it's easy for the user to add his motorcycle specs and it's more readable on the details page.
- Create test cases for the project.
- Complete the registration confirmation process using redis queue.
- Make db queries and views to be cached on a redis cache server.
- Update the auth engine to have more than type of the users so we can have Admin, editors for the website with different roles and access areas.
- Build admin panel to control and monitor the application.
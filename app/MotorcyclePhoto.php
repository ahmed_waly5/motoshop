<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotorcyclePhoto extends Model
{
    protected $fillable = [
        'photo_name',
        'motorcycle_id',
        'user_id',
    ];
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function  motorcycle() {
        return $this->belongsTo('App\Motorcycle');
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Motorcycle;
use App\Http\Requests\MotorcycleRequest;
use App\MotorcyclePhoto;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
class MotorcycleController extends Controller
{
    //
    public function __construct() {
        $this->middleware('auth');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id) {

        $record = Motorcycle::findOrFail($id);
        return view('motorcycle.show', compact('record'));
    }
    public function create() {


        return view('motorcycle.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(MotorcycleRequest $request)
    {

        /**
         * This function was created to solve the issue in Laravel with the boolean field type the field accepts "0" or "1" value but laravel sends "on" and "off"
         *         setCheckbox($request,_field_name);
         */
        $request = setCheckbox($request, 'published');
        $input = $request->all();
        $motorcycle = new Motorcycle($input); // create new motorcycle object
        // Using Auth::user()->motorcycle()->save($motorcycle); to ensure that the authenticated user is the one adding this data, the user_id is excluded from the fillable array in the model for the same reason
        Auth::user()->motorcycles()->save($motorcycle);
        $destination = public_path().'/images'; // upload path
        foreach ($request->photos as $photo) {
            $file = $photo->getClientOriginalName();
            $uploadSuccess = $photo->move($destination, $file);
            if($uploadSuccess){
            }else {
                return redirect('/motorcycle/create');
            }

            MotorcyclePhoto::create([
                'motorcycle_id' => $motorcycle->id,
                'photo_name' => $file
            ]);
        }
        return redirect('/home');
    }


    public function edit($id)
    {
        $record = Motorcycle::find($id);
        return view('motorcycle.edit', compact('record'));
    }

    public function update(MotorcycleRequest $request, $id) {
        $motorcycle = Motorcycle::findOrFail($id);
        $motorcycle = updateCheckbox($request, $motorcycle, 'published');
        $motorcycle = updateCheckbox($request, $motorcycle, 'sold');

        $input = $request->all();

        $motorcycle->update($input);
        $destination = public_path().'/images'; // upload path
        if(!is_null(Input::file('photos'))) {
            foreach ($request->photos as $photo) {
                $file = $photo->getClientOriginalName();
                $uploadSuccess = $photo->move($destination, $file);
                if ($uploadSuccess) {
                } else {
                    return redirect('/motorcycle/edit/');
                }

                MotorcyclePhoto::create([
                    'motorcycle_id' => $motorcycle->id,
                    'photo_name' => $file
                ]);
            }
        }

        return redirect('/home');
    }

    public function destroy($id) {
        $record = Motorcycle::findOrFail($id);
        $record->delete();
        return redirect('/home');
    }

    public function myMotorcycles(){
        $records = Motorcycle::latest()->where('user_id', Auth::user()->id)->get();
        return view('motorcycle.mymotorcycles',compact('records'));
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Motorcycle;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Motorcycle::latest()->where('sold','0')->paginate(6);
        return view('home', compact('records'));
    }
}

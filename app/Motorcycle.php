<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motorcycle extends Model
{
    protected $fillable = [
        'name',
        'description',
        'sold',
        'price',
        'published',
    ];
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function  motorcyclePhotos() {
        return $this->hasMany('App\MotorcyclePhoto');
    }
}

-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 27, 2018 at 01:49 PM
-- Server version: 5.7.19
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rnd_motoshop_09`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2018_02_25_125104_create_motorcycle_table', 2),
(14, '2018_02_25_130000_create_motorcycle_photos_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `motorcycles`
--

DROP TABLE IF EXISTS `motorcycles`;
CREATE TABLE IF NOT EXISTS `motorcycles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(4) NOT NULL,
  `sold` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `motorcycles`
--

INSERT INTO `motorcycles` (`id`, `name`, `description`, `published`, `sold`, `user_id`, `price`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Super 8', 'my super 8 is for sale', 1, 0, 1, 20000, NULL, '2018-02-26 08:45:51', '2018-02-26 14:54:00'),
(2, 'Honda Elite', 'This is my friend\'s honda elite', 1, 0, 1, 19000, NULL, '2018-02-26 11:37:35', '2018-02-26 11:37:35'),
(3, 'Quadro 4', 'Engine:\r\nSingle cylinder, 4 stroke, 4 valve\r\nDisplacement346cc\r\nCooling system Liquid\r\nMaximum power 22.9 kW @ 7500 rpm\r\nMaximum torque 34.8 Nm @ 5500 rpm\r\nFuel supply Electronic injection', 1, 0, 2, 100000, NULL, '2018-02-27 11:26:48', '2018-02-27 11:26:48');

-- --------------------------------------------------------

--
-- Table structure for table `motorcycle_photos`
--

DROP TABLE IF EXISTS `motorcycle_photos`;
CREATE TABLE IF NOT EXISTS `motorcycle_photos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `motorcycle_id` int(10) UNSIGNED NOT NULL,
  `photo_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `motorcycle_photos_motorcycle_id_foreign` (`motorcycle_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `motorcycle_photos`
--

INSERT INTO `motorcycle_photos` (`id`, `motorcycle_id`, `photo_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '880-kymco-super-8-50-2t.jpg', NULL, '2018-02-26 08:45:51', '2018-02-26 08:45:51'),
(2, 1, '2012-Kymco-Super8502Tb.jpg', NULL, '2018-02-26 08:45:51', '2018-02-26 08:45:51'),
(3, 1, 'kymco-super-8-125-specification.jpg', NULL, '2018-02-26 08:45:51', '2018-02-26 08:45:51'),
(4, 2, 'images.jpg', NULL, '2018-02-26 11:37:35', '2018-02-26 11:37:35'),
(5, 2, 'scooter_honda_elite_125_cc_2010_97864112604199140.jpg', NULL, '2018-02-26 11:37:35', '2018-02-26 11:37:35'),
(6, 3, 'Quadro4-Steinbock-02-2.png', NULL, '2018-02-27 11:26:48', '2018-02-27 11:26:48'),
(7, 3, 'Quadro4-Steinbock-03-2.png', NULL, '2018-02-27 11:26:48', '2018-02-27 11:26:48'),
(8, 3, 'Quadro4-Steinbock-04-1.png', NULL, '2018-02-27 11:26:48', '2018-02-27 11:26:48');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ahmed Waly', '01016565558', 'ahmed.waly@yalleksab.com', '$2y$10$cfGc4Yd/ULf5u3EBmBWinOustT5HBDqwYDPAVP0vT4GNgC0yqdOe2', 'qSnqiafrq4011gD2rFsrA92H9EksLVgrEDwfMYfT0ukOOlD80XSaIuXkf3Bx', '2018-02-23 20:50:25', '2018-02-23 20:50:25'),
(2, 'test', '0123456789', 'test@test.com', '$2y$10$r5oSkZu4dIxKT5y0kRZDa.7C0XFVdvt06O1AI6dIWv32G6Own0Av2', 'IqrN8Dfq5rmPMUkOhm871fqbnNVVBbfSjUC4Zx9ngk0tA6VfIRSqRS6LKZok', '2018-02-27 11:19:55', '2018-02-27 11:19:55');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
